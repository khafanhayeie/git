import java.io.IOException;
import java.net.Socket;


public class CA1 {

	public static void main(String[] args) throws IOException{
		
		MainServer server= new MainServer(9083);
		new ServerConsoleReader();
		while (true){
			Socket client= server.accept();
			System.out.println("client accepted");
			if(client!=null){
				new ServiceProvider(client);
			}
		}

	}

}