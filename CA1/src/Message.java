import java.util.ArrayList;

public class Message {
	
	public Message(String cmd, ArrayList<String> params) {
		type = cmd;
		parameters = params;
	}

	public ArrayList<String> getParameters() {
		return parameters;
	}
	
	public String getType() {
		return type;
	}
	
	String type = null;
	ArrayList<String> parameters = null;

}
