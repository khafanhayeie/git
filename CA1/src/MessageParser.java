import java.util.ArrayList;
import java.util.StringTokenizer;

public class MessageParser {
	private static MessageParser ref = null;
	
	private MessageParser(){}
	
	public static MessageParser Get(){
		
		if (ref == null){
			ref = new MessageParser();
			return ref;
		}
		else
			return ref;
		
	}
	public Message parse(String msgText){
		StringTokenizer st = new StringTokenizer(msgText);
		String cmdType = new String();
		ArrayList<String> parameters = new ArrayList<String>();
		
		cmdType = st.nextToken();
		while(st.hasMoreTokens())
			parameters.add(st.nextToken());
		
		Message msg = new Message(cmdType, parameters);
		
		return msg;
	}
}
