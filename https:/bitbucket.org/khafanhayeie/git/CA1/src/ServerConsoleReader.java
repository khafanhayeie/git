import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ServerConsoleReader implements Runnable{
	Thread t= new Thread(this);
	Scanner input= new Scanner (System.in);
	public ServerConsoleReader() {
		t.start();
	}
	Socket connect() {
		Socket s= null;
		try {
			s= new Socket("takna.ut.ac.ir",8080);
			//pw=  new PrintWriter(s.getOutputStream(),true);
			//br= new BufferedReader( new InputStreamReader(s.getInputStream()));


		} catch (IOException e) {
			System.err.println("connection to secondary server failed.");
		}
return s;
	}
	PrintWriter setWriter(Socket s){
		PrintWriter pw=null;
		try {
			pw = new PrintWriter(s.getOutputStream(),true);
		} catch (IOException e) {	
			System.err.println("connection to secondary server failed.");
		}
		return pw;
	}
	
	
	BufferedReader setReader(Socket s){
		BufferedReader br= null;
		try {
			 br= new BufferedReader( new InputStreamReader(s.getInputStream()));
		} catch (IOException e) {
			System.err.println("connection to secondary server failed.");
		}
		return br;
	}
	@Override
	public void run(){
		Socket sock= null;
		PrintWriter requestWriter=null;
		BufferedReader reader= null;
		 Parser parser= new Parser();
		while(true){
			String command= input.nextLine();
			if(command.equals("GET ALL")){
				
				 sock= connect();
			     requestWriter= setWriter(sock);
				 reader= setReader(sock);
				 requestWriter.println(command);
				 String response=null;
				 try {
			//////////
					 response= reader.readLine();
						 parser.parseAll(response);
						 sock.close();
						 requestWriter.close();
						 reader.close();
				} catch (IOException e) {
					System.err.println("couldn't get response from server");
				}
				
			}
			else if(command.startsWith("GET ")){
				 sock= connect();
				 requestWriter= setWriter(sock);
				 reader= setReader(sock);
			     requestWriter.println(command);
				 String response=null;
				
					 try {
						 response= reader.readLine();
						 parser.parse(response);
						 sock.close();
						 requestWriter.close();
						 reader.close();
			}

			 catch (IOException e) {
				System.err.println("couldn't get response from server");
			}
			
		
			}
		}
	}
	public void finalize(){
		input.close();
	}

}
/*
BufferedReader from= null;
PrintWriter to= null;
Socket socket = new Socket("takna.ut.ac.ir", 8080);
from=new BufferedReader(new InputStreamReader(socket.getInputStream()));
to=  new PrintWriter(socket.getOutputStream(),true);
to.println("GET ALL");
System.out.println(from.readLine());


*/