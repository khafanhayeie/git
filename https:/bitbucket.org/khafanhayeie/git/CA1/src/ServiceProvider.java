import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class ServiceProvider implements Runnable {
	Socket client=null;
	BufferedReader fromClient= null;
	PrintWriter toClient= null;
	Thread t= new Thread(this);
	public ServiceProvider(Socket client){
			this.client=client;
			try {
				fromClient=new BufferedReader(new InputStreamReader(client.getInputStream()));
			} catch (IOException e) {
				System.err.print("stream failed to set, loc: client,fromClient");
			}
			try {
				toClient=  new PrintWriter(client.getOutputStream(),true);
			} catch (IOException e) {
				System.err.print("stream failed to set, loc: client,toClient");
			}
			t.start();
	}
	@Override
	public void run(){
		while(true){
			String input=null;
			try {
				input = fromClient.readLine();
			} catch (IOException e) {
				System.err.print("error in reading commands,loc: serviceProvider");
			}
			
			String [] tokens= input.split(" ");
			if(tokens.length<1){
				System.out.println("bad command");
				continue;
			}
			}
		//	System.err.print("error in reading from stream,loc: serviceProvider");
		
		
		
	}

}
